;;;; pt-timer.asd

(asdf:defsystem #:pt-timer
  :description "Describe pt-timer here"
  :author "Gabriel Pariat <gabrielpariat@gmail.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:unix-opts
               #:harmony-simple)
  :components ((:file "package")
               (:file "pt-timer"))
  :build-operation "program-op"
  :build-pathname "pt-timer"
  :entry-point "pt-timer:main")
