;;;; pt-timer.lisp

(in-package #:pt-timer)

(defvar *time-units*
  '((d . 86400)
    (h . 3600)
    (m . 60)
    (s . 1)))

(defvar *assets-path* (asdf:system-relative-pathname :pt-timer #p"assets/"))

(opts:define-opts
    (:name :help
           :description
           "Enter the time for the timer in this format: 20d5h3m4s.
You can also add multiple times that will run in a sequence."
           :short #\h
           :long "help")
    (:name :repeat
           :description
           "Enter the number of time to repeat"
           :short #\r
           :arg-parser #'parse-integer
           :long "repeat"))

(defun get-sleep-time (time)
  (or (cdr (find time *time-units* :test #'> :key #'cdr)) 1))

(defun parse-time-part (time-part)
  (let* ((len (length time-part))
         (qty (parse-integer (subseq time-part 0 (1- len))))
         (unit (subseq time-part (1- len) len))
         (unit-time (cdr (assoc unit *time-units* :test #'string-equal))))
    (* qty unit-time)))

(defun parse-time-parts (time-parts)
  (reduce (lambda (total-times time-part)
            (+ total-times (parse-time-part time-part)))
          (mapcar #'string time-parts)
          :initial-value 0))

(defun time-unit->string (time unit)
  (let ((units (floor (/ time (cdr unit))))
        (remaining (mod time (cdr unit))))
    (values (when (> units 0) (format nil "~a~a" units (car unit)))
            remaining)))

(defun time->string (time)
  (flet ((format-time (units)
           (string-trim '(#\Space) (format nil "~{~(~a~) ~}" units)))
         (format-time-part (units-remaining unit)
           (multiple-value-bind (time-units remaining)
               (time-unit->string (cdr units-remaining) unit)
             (cons (if time-units
                       (cons time-units (car units-remaining))
                       (car units-remaining))
                   remaining)
           )))
    (format-time (reverse (car (reduce #'format-time-part
                                       *time-units*
                                       :initial-value (cons () time)))))))

(defun run-timers (timer-times repeat)
  (loop for timer-time in timer-times
       do (run-timer timer-time))
  (when (and repeat (> repeat 0))
    (run-timers timer-times (1- repeat))))

(defun run-timer (timer-time)
  (let* ((start (get-universal-time))
         (end (+ start timer-time)))
    (progn (loop while (< (get-universal-time) end)
       do (let* ((now (get-universal-time))
                 (remaining (- end now))
                 (sleep-time (get-sleep-time remaining)))
            (progn (format t
                           "~C~a"
                           #\Return
                           (time->string remaining))
                   (force-output)
                   (sleep sleep-time))))
           (format t "~C~a~%" #\Return "Done!")
           (harmony-simple:play (merge-pathnames *assets-path* "ding.mp3") :sfx)
           (sleep 1)
           )))

(defun parse-time (string)
  (loop for i = 0 then (1+ j)
     as j = (position-if #'alpha-char-p string :start i)
     when j
     sum (* (parse-integer (subseq string i j))
                (cdr (assoc (char string j) *time-units* :test #'string-equal)))
     while j))

(defun parse-args (args)
  (map 'list #'parse-time (map 'list #'string args)))

(defun init ()
  (harmony-simple:initialize))

(defun main ()

  (multiple-value-bind (options free-args)
      (opts:get-opts)
    (if (getf options :help)
        (progn
          (opts:describe
           :prefix "You're in pt-timer. Usage:"
           :args "[keywords]")
          (opts:exit))
        (progn
          (init)
          (handler-case 
              (run-timers (parse-args free-args) (getf options :repeat))
            ;; Catch a user's C-c
            (#+sbcl sb-sys:interactive-interrupt
              #+ccl  ccl:interrupt-signal-condition
              #+clisp system::simple-interrupt-condition
              #+ecl ext:interactive-interrupt
              #+allegro excl:interrupt-signal
              ()
              (opts:exit)))
          (opts:exit)))))
