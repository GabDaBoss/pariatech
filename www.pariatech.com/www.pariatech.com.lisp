;;;; www.pariatech.com.lisp

(in-package #:www.pariatech.com)

(defun xhtml (el)
  (format t "<!DOCTYPE html>")
  (xml el))

(defun logo (s)
  (let ((r (/ s 5)))
    `(svg :width ,s :height ,s
          (rect :fill "red" :x 0 :y 0 :width ,r :height ,(* 5 r))
          (rect :fill "red" :x ,r :y 0 :width ,r :height ,r)
          (rect :fill "red" :x ,r :y ,(* 2 r) :width ,r :height ,r)
          (rect :fill "red" :x ,(* 2 r) :y ,r :width ,r :height ,(* 2 r))
          (rect :fill "black" :x ,(* 2 r) :y 0 :width ,(* 3 r) :height ,r)
          (rect :fill "black" :x ,(* 3 r) :y ,r :width ,r :height ,(* 4 r)))))

(defvar head `(head (meta :charset "UTF-8")
                    (meta :name "viewport"
                          :content "width=device-width,initial-scale=1, shrink-to-fit=no")
                    (link :rel "stylesheet" :href "/css/styles.css")
                    (title "Pariatech")))

(defun link (active-path path label)
  `(a :href ,path
      ,(if (equalp active-path (subseq path 1))
           `(b ,label)
           label)))

(defun header (path)
  `(header (div :class "title"
                (a :class "logo"
                   :href "/"
                   ,(logo 30)
                   (strong www.pariatech.com)))
           (hr :class "hidden")
           (nav ,(link path "/" "Home") "&nbsp;"
                ,(link path "/fitness" "Fitness") "&nbsp;"
                ,(link path "/tech" "Tech"))
           (hr :class "hidden")))

(defun aside (path paths)
  `(aside (ul ,@(loop for i in paths
                      collect `(li ,(link path (car i) (cdr i)))))))

(defun page-template (path aside-path aside-paths &rest main)
  (xhtml
   `(html :lang "en"
           ,head
           (body ,(header path)
                 ,(aside aside-path aside-paths)
                 (main ,@main)
                 (footer)))))

(defpage index :route ""
  (page-template "" ""
                 '(("/" . "About") ("/contact" . "Contact"))
                 `(article (hgroup (h1 "Welcome to Pariatech")
                                   (h2 "The Home of a Tech'n'Fitness Enthusiast"))
                           (p "Welcome my name is Gabriel Pariat and here is my website to share my
love of tech and fitness. Enjoy your stay!"))))

(defpage contact :route "contact"
  (page-template "" "contact"
                 '(("/" . "About") ("/contact" . "Contact"))
                 `(article (h1 "Contact")
                           (p "You can contact me through this "
                              (a :href "mailto:gabrielpariat@gmail.com" "email")
                              " if you have any question or business inquiry."
                              "I'll be happy to answer!"))))


(defun path->route (dir path)
  (let* ((path (replace-all (namestring path) (namestring dir) ""))
         (route (subseq path 0 (position #\. path :from-end t))))
    (subseq route 0 (search "/index" route))))

(defun path->page (pages-dir path)
  `((:route ,(path->route pages-dir path))
    (:page ,@(parse-org (read-org-file path)))))

(defun get-pages-files (pages-dir)
  (mapcan #'uiop:directory-files
          (labels ((f (lst dirs)
                     (let ((dir (car lst)))
                       (if dir
                           (f (cdr lst)
                              (f (uiop:subdirectories dir)
                                 (cons dir dirs)))
                           dirs))))
            (f (list pages-dir) nil))))

(defun get-pages (pages-dir)
  (let* ((pages-dir (car (uiop:directory* pages-dir)))
         (pages-files (get-pages-files pages-dir)))
    (mapcar (lambda (file) (path->page pages-dir file)) pages-files)))

(defun page->route (page)
  (mapcar (lambda (x) (intern (string-upcase x)))
          (uiop:split-string
           (cadr (assoc :route page))
           :separator "/")))

(defun header-nav (cur pages)
  (let ((cur-route (car (page->route cur)))
        (routes (remove-duplicates
                 (mapcar (lambda (page)
                           (car (page->route page)))
                         pages))))
    (loop for route in routes
          collect `(a :href ,(format nil "/~(~a~)" route)
                      :style ,(inline-css `(:font-weight
                                          ,(if (eq cur-route route)
                                              'bold
                                              'normal)))
                      ,route))))

(defun article-aside (cur pages)
  (let* ((cur-route (page->route cur))
         (routes (sort (loop for page in pages
                             for route = (page->route page)
                             when (eq (car cur-route) (car route))
                               collect route)
                       #'<
                       :key #'length)))
    `(aside (ul ,@(loop for route in routes
                        collect `(li (a :href ,(format nil "/~{~(~a~)~^/~}" route)
                                        :style ,(inline-css `(:font-weight
                                                              ,(if (equal cur-route route)
                                                                   'bold
                                                                   'normal)))
                                        ,(car (last route)))))))))

(defun article-page (page all)
  (xhtml `(html :lang "en"
                ,head
                (body (header (div :class "title"
                                   (a :class "logo"
                                      :href "/"
                                      ,(logo 30)
                                      (strong www.pariatech.com)))
                              (hr :class "hidden")
                              (nav ,@(header-nav page all)))
                      ,(article-aside page all)
                      (main (article ,@(assoc* '(:page article)
                                               page)))
                      (footer)))))

(defun create-sitetree (pages)
  (let ((routes (loop for page in pages
                      collect (cons (cadr (assoc :route page))
                                    (assoc* '(:page header title) page)))))
    routes))

(defun create-pages ()
  (let ((pages (get-pages "./pages/")))
    (labels ((f (pages all)
               (let ((page (car pages)))
                 (when page
                   (progn
                     (defpage (gensym) :route (cadr (assoc :route page))
                       (article-page page all))
                     (f (cdr pages) all))))))
      (f pages pages))))

(defstylesheet styles
  (css
   `((body :margin 0
           :font-family "Roboto, monospace"
           :background-color "#282828"
           :color "#fff")
     (a :color "#fff")
     ("header a" :text-decoration none)
     ("header a.logo" :font-size 30px)
     ("header .title" :padding 0.5em)
     ("header nav" :background-color "#000"
                   :padding "0.5em 1em")
     ("header nav a" :color "#fff"
                     :padding "0.5em 1em")
     ("header nav a:hover" :box-shadow "inset 0 0 999px rgba(255, 255, 255, 0.2)")
     (main :padding "0 1em")
     ("@media screen and (min-width: 768px)"
      (aside :float left
             :width 10em
             :border-right "1px dotted #444")
      (main :margin "0 0 0 10em"
            :padding "0 2em"))
     ("aside ul" :padding 0)
     ("aside ul li" :list-style none)
     ("aside ul li a" :text-decoration none
                      :display block
                      :color "#fff"
                      :padding "0.5em 1em")
     ("aside ul li a:hover" :box-shadow "inset 0 0 999px rgba(255, 255, 255, 0.2)")

     (".hidden" :display none))))
