(ns ^:figwheel-hooks pariatech.spaceship)

(def canvas (.getElementById js/document "screen"))
(def context (.getContext canvas "2d"))
(def meshCube [[{:x 0 :y 0 :z 0} {:x 0 :y 1 :z 0} {:x 1 :y 1 :z 0}] ;; South
               [{:x 0 :y 0 :z 0} {:x 1 :y 1 :z 0} {:x 1 :y 0 :z 0}] ;; South
               [{:x 1 :y 0 :z 0} {:x 1 :y 1 :z 0} {:x 1 :y 1 :z 1}] ;; East
               [{:x 1 :y 0 :z 0} {:x 1 :y 1 :z 1} {:x 1 :y 0 :z 1}] ;; East
               [{:x 1 :y 1 :z 0} {:x 0 :y 0 :z 0} {:x 0 :y 1 :z 1}] ;; Top
               [{:x 1 :y 1 :z 0} {:x 0 :y 1 :z 1} {:x 1 :y 1 :z 1}] ;; Top
               [{:x 0 :y 1 :z 0} {:x 0 :y 1 :z 1} {:x 0 :y 0 :z 1}] ;; West
               [{:x 0 :y 1 :z 0} {:x 0 :y 0 :z 1} {:x 0 :y 0 :z 0}] ;; West
               [{:x 0 :y 1 :z 1} {:x 1 :y 1 :z 1} {:x 1 :y 0 :z 1}] ;; North
               [{:x 0 :y 1 :z 1} {:x 1 :y 0 :z 1} {:x 0 :y 0 :z 1}] ;; North
               [{:x 0 :y 0 :z 1} {:x 1 :y 0 :z 1} {:x 1 :y 0 :z 0}] ;; Bottom
               [{:x 0 :y 0 :z 1} {:x 1 :y 0 :z 0} {:x 0 :y 0 :z 0}]]) ;; Bottom

(def width (.-offsetWidth canvas))
(def height (.-offsetHeight canvas))
(def fov 90)
(def zfar 1000)
(def znear 0.1)

(def f (/ 1 (js/Math.tan (* (/ fov 2 180) js/Math.PI))))
(def a (/ width height))
(def q (/ zfar (- zfar znear)))

(def projection-matrix [[(* a f) 0 0 0]
                        [0 f 0 0]
                        [0 0 q 1]
                        [0 0 (* -1 znear q) 0]])

(defn multiply-matrix-vector [vec mat]
  (let* [w (+ (* (:x vec) ((mat 0) 3))
              (* (:y vec) ((mat 1) 3))
              (* (:z vec) ((mat 2) 3))
              ((mat 3) 3))
         w (if (= w 0) 1 w)]
    {:x (/ (+ (* (:x vec) ((mat 0) 0))
               (* (:y vec) ((mat 1) 0))
               (* (:z vec) ((mat 2) 0))
               ((mat 3) 0))
            w)
      :y (/ (+ (* (:x vec) ((mat 0) 1))
               (* (:y vec) ((mat 1) 1))
               (* (:z vec) ((mat 2) 1))
               ((mat 3) 1))
            w)
      :z (/ (+ (* (:x vec) ((mat 0) 2))
               (* (:y vec) ((mat 1) 2))
               (* (:z vec) ((mat 2) 2))
               ((mat 3) 1))
            w)
      }))

(set! (.-width canvas) width)
(set! (.-height canvas) height)
(set! (.-strokeStyle context) "#FF0000")
(set! (.-fillStyle context) "#FF0000")
(.moveTo context 0 0)
(.lineTo context 200 100)
(.stroke context)

(js/console.log "hello!")

(defn scale-to-screen [vertice]
  {:x (-> (:x vertice) (+ 1) (/ 2) (* width))
   :y (-> (:y vertice) (+ 1) (/ 2) (* height))
   :z (:z vertice)})

(defn draw-line [start end]
  (.beginPath context)
  (.moveTo context (:x start) (:y start))
  (.lineTo context (:x end) (:y end))
  (.stroke context))

(defn multiply-matrix-triangle [triangle matrix]
  (vec (for [vertice triangle]
         (multiply-matrix-vector vertice matrix))))

(defn project-triangle [triangle]
  (multiply-matrix-triangle triangle projection-matrix))

(defn scale-to-screen-triangle [triangle]
  (vec (for [vertice triangle]
         (scale-to-screen vertice))))

(defn translate-triangle [triangle x y z]
  (vec
   (for [vertice triangle]
     (assoc vertice
            :x (+ (:x vertice) x)
            :y (+ (:y vertice) y)
            :z (+ (:z vertice) z)))))


(defn draw-mesh [mesh]
  )

(defn mat-rot-z [theta]
  [[(js/Math.cos theta)     (js/Math.sin theta) 0 0]
   [(- (js/Math.sin theta)) (js/Math.cos theta) 0 0]
   [0                       0                   1 0]
   [0                       0                   0 1]])

(defn mat-rot-x [theta]
  [[1 0                               0                           0]
   [0 (js/Math.cos (* theta 0.5))     (js/Math.sin (* theta 0.5)) 0]
   [0 (- (js/Math.sin (* theta 0.5))) (js/Math.cos (* theta 0.5)) 0]
   [0 0                               0                           1]])

(defn triangle-line [triangle start end]
  (let [start-vertice (nth triangle start)
        end-vertice (nth triangle end)]
    {:x (- (:x end-vertice) (:x start-vertice))
     :y (- (:y end-vertice) (:y start-vertice))
     :z (- (:z end-vertice) (:z start-vertice))}))

(defn triangle-normal [triangle]
  (let [line1 (triangle-line triangle 0 1)
        line2 (triangle-line triangle 2 0)
        normal {:x (- (* (:y line1) (:z line2)) (* (:z line1) (:y line2)))
                :y (- (* (:z line1) (:x line2)) (* (:x line1) (:z line2)))
                :z (- (* (:x line1) (:y line2)) (* (:y line1) (:x line2)))}
        length (js/Math.sqrt (+ (js/Math.pow (:x normal) 2)
                                (js/Math.pow (:y normal) 2)
                                (js/Math.pow (:z normal) 2)))]
    {:x (/ (:x normal) length)
     :y (/ (:y normal) length)
     :z (/ (:z normal) length)}))

(defn render [timestamp]
  (.clearRect context 0 0 (.-width canvas) (.-height canvas))
  (doseq [triangle meshCube]
    (let* [theta (/ timestamp 1000)
           transformed-tri (-> triangle
                               (multiply-matrix-triangle (mat-rot-z theta))
                               (multiply-matrix-triangle (mat-rot-x theta))
                               (translate-triangle 0 0 3)
                               project-triangle
                               scale-to-screen-triangle)]
      (draw-line (nth transformed-tri 0) (nth transformed-tri 1))
      (draw-line (nth transformed-tri 1) (nth transformed-tri 2))
      (draw-line (nth transformed-tri 2) (nth transformed-tri 0))))
  (js/requestAnimationFrame render))


(def last-timestamp (atom (js/performance.now)))

(js/requestAnimationFrame render)
