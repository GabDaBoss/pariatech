#!/usr/bin/env bash
set -euo pipefail

clj -A:prod:worker-prod

aws s3 sync ./resources/public/ s3://timer.pariatech.com/
aws s3 sync ./dist/ s3://timer.pariatech.com/
