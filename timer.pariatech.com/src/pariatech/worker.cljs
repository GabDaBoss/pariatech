(ns pariatech.worker)

(def CACHE_NAME "pariatech.timer-cache-v1")
(def urls-to-cache ["/"
                    "/style.css"
                    "/app.js"])

(defn install-handler [event]
  (.waitUntil event
              (.then
               (js/caches.open CACHE_NAME)
               (fn [cache] (.addAll cache
                                    urls-to-cache)))))

(defn fetch-response-handler [request]
  (fn [response]
    (if (or (not (nil? response))
            (not (== (.-status response) 200))
            (not (= (.-type response) "basic")))
      response
      (let [response-to-cache (.clone response)]
        (-> (js/caches.open CACHE_NAME)
            (.then #(.put % request response-to-cache)))
        response))))

(defn caches-match-handler [request]
  (fn [response]
    (if-not (nil? response)
      response
      (-> (js/fetch request)
          (.then (fetch-response-handler request))))))

(defn fetch-handler [event]
  (let [request (.-request event)]
    (.respondWith event
                  (-> (js/caches.match request)
                      (.then (caches-match-handler request))))))

(js/self.addEventListener "install" install-handler)
(js/self.addEventListener "fetch" fetch-handler)
