(ns ^:figwheel-hooks pariatech.timer
  (:require
   [reagent.dom :as r.dom]
   [reagent.core :as r]
   [alandipert.storage-atom :refer [local-storage]]))

(declare start-timer)
(declare timer->string)
(declare stop-all)

(def state
  (local-storage
   (r/atom
    {:active-sequence 0
     :timer-sequences
     [{:name "Scratch Pad"
       :timers
       [{:days nil
         :hours nil
         :minutes nil
         :seconds nil
         :remaining nil
         :then nil
         :interval-id nil
         :running false
         :paused false}]
       :master-controls
       {:running false
           :paused false
           :mode :sequential
           :looping false}
       }]
     })
   :pariatech.timer))

(defn change-active-sequence! [i]
  (swap! state assoc :active-sequence i))

(defn add-timer-sequence!
  ([name]
   (add-timer-sequence! name [{:days nil
        :hours nil
        :minutes nil
        :seconds nil
        :remaining nil
        :then nil
        :interval-id nil
        :running false
        :paused false}]))
  ([name timers]
   (stop-all)
   (let [length (count (:timer-sequences @state))]
     (swap! state
            assoc-in
            [:timer-sequences length]
            {:name name
             :timers
             timers
             :master-controls
             {:running false
              :paused false
              :mode :sequential
              :looping false}
             })
     (swap! state assoc :active-sequence length))))

(defn update-timer-kv [state i k v]
  (assoc-in state
            [:timer-sequences
             (:active-sequence state)
             :timers
             i
             k]
            v))

(defn update-timer [state i & body]
  (reduce #(apply update-timer-kv %1 i %2) state (partition 2 body)))

(defn update-timer! [i & body]
  (swap! state #(apply update-timer % i body)))

(defn add-timer [state]
  (let* [active-timer (:active-sequence state)
         length (count (:timers (nth (:timer-sequences state) active-timer )))]
    (assoc-in state
              [:timer-sequences active-timer :timers length]
              {:days nil
               :hours nil
               :minutes nil
               :seconds nil
               :remaining nil
               :then nil
               :interval-id nil
               :running false
               :paused false})))

(defn add-timer! [] (swap! state add-timer))

(defn remove-at
  "remove elem in coll"
  [coll pos]
  (vec (concat (subvec coll 0 pos) (subvec coll (inc pos)))))

(defn remove-timer [state i]
  ;;(swap! state remove-at i)
  (assoc-in state [:timer-sequences (:active-sequence state) :timers]
            (remove-at (get-in state [:timer-sequences
                                      (:active-sequence state)
                                      :timers])
                       i)))

(defn remove-timer! [i] (swap! state #(remove-timer % i)))


(defn active-timers
  ([state]
   (:timers (nth (:timer-sequences state) (:active-sequence state) )))
  ([] (active-timers @state)))

(defn nth-active-timers
  [i]
  (nth (active-timers) i))

(defn update-active-master-controls! [key value]
  (swap! state
         assoc-in
         [:timer-sequences
          (:active-sequence @state)
          :master-controls
          key]
         value))

(defn update-master-controls! []
  (update-active-master-controls!
   :running
   (some #(:running %) (active-timers))))

(defn set-active-master-controls-to-running! []
  (update-active-master-controls! :running true))

(defn set-active-master-controls-to-paused! []
  (update-active-master-controls! :paused true)
  (update-active-master-controls! :running false))

(defn set-active-master-controls-to-stopped! []
  (update-active-master-controls! :paused false)
  (update-active-master-controls! :running false))

(defn set-active-master-controls-to-restart! []
  (update-active-master-controls! :paused false)
  (update-active-master-controls! :running (> (count (active-timers)) 0)))

(defn set-active-master-controls-mode! [mode]
  (update-active-master-controls! :mode mode))

(defn set-active-master-controls-looping! [looping]
  (update-active-master-controls! :looping looping))

(defn active-master-controls []
  (get-in @state
          [:timer-sequences
           (:active-sequence @state)
           :master-controls]))

(defn if-sequential [sequential-fn parallel-fn]
  (case (:mode (active-master-controls))
    :sequential (sequential-fn)
    :parallel (parallel-fn)))

(defn if-looping [looping-fn not-looping-fn]
  (if (:looping (active-master-controls))
    (looping-fn)
    (not-looping-fn)))

(defn timer->remaining [timer]
  (* (+ (* (:days timer) 86400)
        (* (:hours timer) 3600)
        (* (:minutes timer) 60)
        (:seconds timer))
     1000))

(defn remaining->timer [remaining]
  (let [total-seconds (/ remaining 1000)
        days (Math/floor (/ total-seconds 86400))
        hours (Math/floor (/ (mod total-seconds 86400) 3600))
        minutes (Math/floor (/ (mod total-seconds 3600) 60))
        seconds (mod (Math/round total-seconds) 60)]
    {:days days
     :hours hours
     :minutes minutes
     :seconds seconds}))

(defn start-next-timer [i]
  (when (:running (active-master-controls))
    (letfn [(start-next [] (when (> (dec (count (active-timers))) i)
                             (start-timer (inc i))))]
      (if-looping
          #(if (= (dec (count (active-timers))) i)
             (start-timer 0)
             (start-next))
        #(start-next)))))

(defn run-timer [i]
  (let [timer (nth-active-timers i)
        now (.now js/Date)
        remaining (- (:remaining timer) (- now (:then timer)))
        audio (js/document.getElementById "audio")]
    (if (<= remaining 0)
      (do
        (js/clearInterval (:interval-id timer))
        (.play audio)
        (update-timer! i
                      :remaining 0
                      :then nil
                      :interval-id nil
                      :running false)
        (if-sequential #(start-next-timer i)
          #(if-looping (fn [] (start-timer i)) (fn [] ())))
        (update-master-controls!)
        (set! js/document.title "Timer's Done"))
      (do
        (update-timer! i :remaining remaining :then now)
        (set! js/document.title (timer->string (remaining->timer remaining)))))))

(defn start-timer [i]
  (let [remaining (timer->remaining (nth-active-timers i))]
    (update-timer! i
                  :remaining (timer->remaining (nth-active-timers i))
                  :then (.now js/Date)
                  :interval-id (js/setInterval #(run-timer i) 1000)
                  :running true
                  :paused false)
    (set! js/document.title (timer->string (remaining->timer remaining)))))

(defn resume-timer [i]
  (let [remaining (:remaining (nth-active-timers i))]
    (when (:paused (nth-active-timers i))
      (update-timer! i
                    :remaining remaining
                    :then (.now js/Date)
                    :interval-id (js/setInterval #(run-timer i) 1000)
                    :running true
                    :paused false)
      (set! js/document.title (timer->string (remaining->timer remaining))))))

(defn pause-timer [i]
  (let [timer (nth-active-timers i)
        now (.now js/Date)
        remaining (- (:remaining timer) (- now (:then timer)))]
    (when (:running timer)
      (js/clearInterval (:interval-id timer))
      (update-timer! i
                    :remaining remaining
                    :then nil
                    :interval-id nil
                    :paused true
                    :running false)
      (set! js/document.title "Timer's Paused"))))

(defn stop-timer [i]
  (let [timer (nth-active-timers i)]
    (js/clearInterval (:interval-id timer))
    (update-timer! i
                  :remaining nil
                  :then nil
                  :interval-id nil
                  :running false
                  :paused false)
    (set! js/document.title "Timer")))

(defn restart-timer [i]
  (stop-timer i)
  (start-timer i))

(defn timer->string [timer]
  (str
   (:days timer) "d "
   (:hours timer) "h "
   (:minutes timer) "m "
   (:seconds timer) "s"))

(defn countdown-remaining-component [remaining]
  (let [total-seconds (/ remaining 1000)
        days (Math/floor (/ total-seconds 86400))
        hours (Math/floor (/ (mod total-seconds 86400) 3600))
        minutes (Math/floor (/ (mod total-seconds 3600) 60))
        seconds (mod (Math/round total-seconds) 60)]
    [:span
     [:span.time-unit days "d"]
     [:span.time-unit hours "h"]
     [:span.time-unit minutes "m"]
     [:span.time-unit seconds "s"]]))

(defn countdown-component [remaining]
  [:h3.countdown (cond (nil? remaining) "Ready to Start"
                       (<= remaining 0) "Done"
                       :else (countdown-remaining-component remaining))])

(defn timer-input-component [i unit]
  (let [timer (nth-active-timers i)]
    [:input.timer {:type "number"
                   :value (unit timer)
                   :disabled (or (:running timer) (:paused timer))
                   :on-change #(update-timer! i
                                             unit
                                             (-> % .-target .-value))}]))

(defn timer-inputs-component [i]
  [:div.timer-inputs
   [timer-input-component i :days]
   [:div.unit "d"]
   [timer-input-component i :hours]
   [:div.unit "h"]
   [timer-input-component i :minutes]
   [:div.unit "m"]
   [timer-input-component i :seconds]
   [:div.unit "s"]])

(defn controls-component [i]
  (let [timer (nth-active-timers i)]
    [:div.d-flex.justify-content-center.mt-3
     [:button.btn.btn-dark.mr-3
      {:type "button"
       :on-click #(if (:paused timer)
                    (resume-timer i)
                    (start-timer i))
       :class (when (:running timer) "active")
       :disabled (or (:running timer)
                     (:running (active-master-controls))
                     (:paused (active-master-controls)))}
      [:i.fa.fa-play]]
     [:button.btn.btn-dark.mr-3
      {:type "button"
       :on-click #(pause-timer i)
       :class (when (:paused (nth-active-timers i)) "active")
       :disabled (or (:paused timer)
                     (:running (active-master-controls))
                     (:paused (active-master-controls)))}
      [:i.fa.fa-pause]]
     [:button.btn.btn-dark.mr-3
      {:type "button"
       :on-click #(stop-timer i)
       :disabled (or (:running (active-master-controls))
                     (:paused (active-master-controls)))}
      [:i.fa.fa-stop]]
     [:button.btn.btn-dark.mr-3
      {:type "button"
       :on-click #(restart-timer i)
       :disabled (or (:running (active-master-controls))
                     (:paused (active-master-controls)))}
      [:i.fa.fa-refresh]]
     [:button.btn.btn-dark
      {:type "button"
       :on-click #(remove-timer! i)
       :disabled (or (:running (active-master-controls))
                     (:paused (active-master-controls)))}
      [:i.fa.fa-trash]]]))

(defn timer-component [i]
  (let [timer (nth-active-timers i)]
    [:div.mt-4
     [countdown-component (:remaining timer)]
     [timer-inputs-component i]
     [controls-component i]]))

(defn add-timer-btn-component []
  [:button.btn.btn-dark.mt-5.btn-block
   {:on-click
    add-timer!}
   [:i.fa.fa-plus.mr-2]
   "Add Timer"])

(defn timers-component []
  [:div
   [:h1.text-center (get-in @state [:timer-sequences
                                    (:active-sequence @state)
                                    :name])]
   (for [i (range (count (active-timers)))]
     ^{:key i} [timer-component i])])

(defn action-on-all [action]
  (let [len (count (active-timers))]
    (loop [i 0]
      (when (< i len)
        (action i)
        (recur (inc i))))))

(defn start-first-timer []
  (start-timer 0))

(defn start-all-timers []
  (action-on-all start-timer))

(defn start-all []
  (let [timers (active-timers)
        len (count timers)]
    (if (> len 0)
      (do (if-sequential start-first-timer
            start-all-timers)
          (set-active-master-controls-to-running!)))))

(defn pause-all []
  (action-on-all pause-timer)
  (set-active-master-controls-to-paused!))

(defn resume-all []
  (action-on-all resume-timer)
  (set-active-master-controls-to-running!))

(defn stop-all []
  (action-on-all stop-timer)
  (set-active-master-controls-to-stopped!))

(defn restart-all []
  (action-on-all restart-timer)
  (set-active-master-controls-to-restart!))

(defn footer-component []
  [])

(defn master-controls-component []
  (let [expanded (r/atom false)]
    (fn []
      [:div.d-flex.flex-column.mt-3.master-controls
       [:div.d-flex
        [:div.d-flex.flex-grow-1.justify-content-center.center-controls
         [:button.btn.btn-dark
          {:type "button"
           :on-click #(if (:paused (active-master-controls))
                        (resume-all)
                        (start-all))
           :disabled (:running (active-master-controls))}
          [:i.fa.fa-play]]
         [:button.btn.btn-dark.ml-3
          {:type "button"
           :on-click pause-all}
          [:i.fa.fa-pause]]
         [:button.btn.btn-dark.ml-3
          {:type "button" :on-click stop-all}
          [:i.fa.fa-stop]]
         [:button.btn.btn-dark.ml-3
          {:type "button" :on-click restart-all}
          [:i.fa.fa-refresh]]
         ]
        [:button.btn.btn-dark
         {:type "button"
          :class (when (:expanded (active-master-controls)) "active")
          :on-click #(swap! expanded not)}
         [:i.fa.fa-bars]]]
       (when @expanded
         [:div.expanded-controls.d-flex.justify-content-center
          [:div.row
           [:div.col-12.col-sm.d-flex.mt-3
            [:label.d-flex.justify-content-center.flex-column.mb-0
             {:for "mode"}
             "Mode"]
            [:select.ml-3.custom-select.mode-select
             {:id "mode"
              :name "mode"
              :on-change #(set-active-master-controls-mode!
                           (-> % .-target .-value keyword))}
             [:option {:value :sequential} "Sequential"]
             [:option {:value :parallel} "Parallel"]]]
           [:div.col-12.col-sm.mt-3.d-flex.justify-content-center.flex-column
            [:div.custom-control.custom-checkbox
             [:input.custom-control-input
              {:type "checkbox"
               :id "looping"
               :name "looping"
               :checked (:looping (active-master-controls))
               :on-change #(set-active-master-controls-looping!
                            (-> % .-target .-checked))}]
             [:label.custom-control-label
              {:for "looping"}
              "Looping"]]]]])])))

(defn sequence-name-modal [id title btn-txt on-click]
  (let [name (r/atom "")]
    (fn []
      [:div.modal.fade
       {:id id
        :tabIndex "-1"
        :role "dialog"
        :aria-labelledby "sequenceNameModalLabel"
        :aria-hidden "true"}
       [:div.modal-dialog {:role "document"}
        [:div.modal-content
         [:div.modal-header.bg-dark
          [:h5#sequenceNameModalLabel.modal-title
           title]
          [:button.close
           {:type "button"
            :data-dismiss "modal"
            :aria-label "Close"}
           [:span
            {:aria-hidden "true"
             :dangerouslySetInnerHTML
             {:__html "&times;"}}]]]
         [:div.modal-body
          [:div.input-group
           [:div.input-group-prepend
            [:span#sequence-name.input-group-text
             "Name"]]
           [:input.form-control
            {:type "text"
             :aria-describedby "sequence-name"
             :value @name
             :on-change #(reset! name (-> % .-target .-value))}]]]
         [:div.modal-footer
          [:button.btn.btn-secondary
           {:type "button"
            :data-dismiss "modal"}
           "Close"]
          [:button.btn.btn-primary
           {:type "button"
            :data-dismiss "modal"
            :on-click #(on-click @name)}
           btn-txt]
          ]]]])))

(defn create-new-sequence []
  [:li.nav-item
   [:a.nav-link
    {:href "#"
     :data-toggle "modal"
     :data-target "#createNewSequenceModal"} "Create New"]
   ])

(defn copy-sequence-as []
  [:li.nav-item
   [:a.nav-link
    {:href "#"
     :data-toggle "modal"
     :data-target "#copySequenceAsModal"} "Copy As"]
   ])

(defn install-btn []
  (let [before-install-prompt-event (r/atom nil)]
    (js/window.addEventListener "beforeinstallprompt"
                                #(do (println %)
                                  (.preventDefault %)
                                  (reset! before-install-prompt-event %)))
    (js/window.addEventListener "appinstalled"
                                #(reset! before-install-prompt-event nil))
    (fn []
      (when-let [e @before-install-prompt-event]
        [:li.nav-item
         [:a.nav-link
          {:href "#"
           :on-click #(.prompt e)}
          "Install App"]]))))

(defn navbar []
  [:nav.navbar.navbar-expand-md.navbar-dark
    [:a.navbar-brand {:href "#"} [:img {:src "pariatech.timer.logo.png"}]]
    [:button.navbar-toggler
     {:type "button"
      :data-toggle "collapse"
      :data-target "#navbarSupportedContent"
      :aria-controls "navbarSupportedContent"
      :aria-expanded "false"
      :aria-label "Toggle navigation"}
     [:span.navbar-toggler-icon]]
    [:div.collapse.navbar-collapse
     {:id "navbarSupportedContent"}
     [:ul.navbar-nav.mr-auto
      [create-new-sequence]
      [copy-sequence-as]
      [:li.nav-item.dropdown
       [:a.nav-link.dropdown-toggle
        {:href "#"
         :id "navbarDropdown"
         :role "button"
         :data-toggle "dropdown"
         :aria-haspopup "true"
         :aria-expanded "false"}
        "Sequences"]
       [:div.dropdown-menu
        {:aria-labelledby "navbarDropdown"}
        (let [timer-sequences (:timer-sequences @state)]
          (for [i (range (count timer-sequences))
                :let [timer (nth timer-sequences i)]]
            ^{:key (:name timer)}
            [:a.dropdown-item
             {:href "#"
              :on-click #(change-active-sequence! i)}
             (:name timer)]) )
        ]]
      [install-btn]]]
    ])

(defn app []
  [:div.w-100.d-flex.flex-column
   [navbar]
   [:audio#audio {:preload "auto"
                  :controls "none"
                  :src "ding.mp3"
                  :style {:display "none"}}]
   [sequence-name-modal
    "createNewSequenceModal"
    "Create New"
    "Create New"
    add-timer-sequence!]
   [sequence-name-modal
    "copySequenceAsModal"
    "Copy As"
    "Copy As"
    #(add-timer-sequence! % (:timers ((:active-sequence @state)
                                      (:timer-sequences state))))]
   [:div.container.flex-grow-1
    (timers-component)
    [add-timer-btn-component]]
   [master-controls-component]])

(defn mount []
  (r.dom/render [app] (js/document.getElementById "root")))

(defn ^:after-load re-render [] (mount))

(defonce start-up (do (mount) true))

(when-let [serviceWorker (.-serviceWorker js/navigator)]
  (js/window.addEventListener "load"
                              (fn []
                                (.register serviceWorker
                                           "/sw.js"))))
